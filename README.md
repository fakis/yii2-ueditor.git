Yii2 UEditor
=========
> 富文本编辑器，针对Yii2框架封装的百度UEditor富文本编辑器

构建于UEditor 版本`ueditor1_4_3_3-utf8-php.zip`
- [Releases](https://github.com/fex-team/ueditor/releases)
- [Github](http://github.com/fex-team/ueditor)
- [官方文档](http://fex-team.github.io/ueditor/)

安装
---

通过 [composer](http://getcomposer.org/download/) 安装此扩展

```
php composer.phar require --prefer-dist fakis/yii2-ueditor "*"
```

或者在`composer.json`文件的require部分填上

```
"fakis/yii2-ueditor": "*"
```

使用
---

### 语言
`ueditor`只支持2种语言，`en-us`和`zh-cn`,默认跟随系统语言 `Yii::$app->language`,可以通过2种方式设置
1. 修改系统语言，在配置文件里添加`'language' => 'zh-CN',`。
2. 实例化的时候配置语言选项

### 配置相关
编辑器相关配置，请在`view` 中配置，参数为`clientOptions`，比如定制菜单，编辑器大小等等，具体参数请查看[UEditor官网文档](http://fex-team.github.io/ueditor/)。

```php
\fakis\ueditor\UEditor::widget([
	// 您可以将它用于模型属性
	'model' => $model,
	'attribute' => 'field',

	// 或者仅用于字段
	'name' => 'input_name',
	
	// 客户端配置
    'clientOptions' => [
        //编辑区域大小
        'initialFrameHeight' => '200',
        //设置语言
        'lang' =>'en', //中文为 zh-cn
        //定制菜单
        'toolbars' => [
            [
                'fullscreen', 'source', 'undo', 'redo', '|',
                'fontsize', 'bold', 'italic', 'underline', 'fontborder', 'strikethrough', '|',
                'forecolor', 'backcolor', '|',
                'justifyleft', 'justifycenter', 'justifyright', '|',
                'lineheight', 'indent', '|',
                'link', 'unlink', '|', 'searchreplace',
                'removeformat', 'formatmatch', '|',
                'insertimage'
            ],
        ]
    ]
]);
```

或者
```php
$form = \yii\widgets\ActiveForm::begin();

echo $form->field($model, 'field')->widget('\fakis\ueditor\UEditor', [
	// 客户端配置
    'clientOptions' => [
        //编辑区域大小
        'initialFrameHeight' => '200',
        //设置语言
        'lang' =>'en', //中文为 zh-cn
        //定制菜单
        'toolbars' => [
            [
                'fullscreen', 'source', 'undo', 'redo', '|',
                'fontsize', 'bold', 'italic', 'underline', 'fontborder', 'strikethrough', '|',
                'forecolor', 'backcolor', '|',
                'justifyleft', 'justifycenter', 'justifyright', '|',
                'lineheight', 'indent', '|',
                'link', 'unlink', '|', 'searchreplace',
                'removeformat', 'formatmatch', '|',
                'insertimage'
            ],
        ]
    ]
]);

\yii\widgets\ActiveForm::end();
```

### 文件上传接口
文件上传相关配置，请在`controller`中配置，参数为`config`,例如文件上传路径等；更多参数请参照 [config.php](https://github.com/BigKuCha/yii2-ueditor-widget/blob/master/config.php) (跟UEditor提供的config.json一样)

```php
public function actions()
{
    return [
        'upload' => [
            'class' => 'fakis\ueditor\UEditorAction',
            'config' => [
                "imageUrlPrefix"  => "http://www.your-domain.com", // 图片访问路径前缀
                "imagePathFormat" => "/upload/image/{yyyy}{mm}{dd}/{time}{rand:6}" // 上传保存路径
                "imageRoot" => Yii::getAlias("@webroot"),
            ],
        ]
    ];
}
```

### 文件上传七牛接口

```php
public function actions()
{
    return [
        'upload' => [
            'class' => 'fakis\ueditor\UEditorAction',
            'config' => [
                'imageUrl' => 'http://up-z2.qiniup.com/', // 七牛区域上传接口域名
                'imageUrlPrefix' => 'http://www.your-domain.com/', // 图片访问路径前缀
                "imagePathFormat" => "/upload/image/{yyyy}{mm}/{uuid}" // 上传保存路径
                'imageFieldName' => 'file', // 上传控件名
            ],
        ]
    ];
}
```