<?php
namespace fakis\ueditor;

use yii\web\AssetBundle;

class UEditorAsset extends AssetBundle
{
    public $sourcePath = '@vendor/fakis/yii2-ueditor/src/assets';

    public $js = [
        'ueditor.config.js',
        'ueditor.all.js',
    ];

    public $depends = [
        'yii\web\YiiAsset'
    ];
}